class Visit {
    constructor() {
        this.specialization = document.querySelector('.js_mod_dd_doc').value;
        this.purpose = document.querySelector('.js_mod_input_purpose').value;
        this.short = document.querySelector('.js_mod_input_short').value;
        this.urgency = document.querySelector('.js_mod_dd_urgency').value;
        this.fullname = document.querySelector('.js_mod_input_fname').value;
        this.status = document.querySelector('.js_mod_dd_status').value;
    }

    sendAndSaveVisit() {

        API.saveCard(this).then(response => {
            visits.push(response);
            Dashboard.renderDashboard(visits);
        });
    }

    sendAndEditVisit(id) {

        visits.splice(visits.findIndex(v => v.id === id), 1);

        API.editCard({...this, id }).then(response => {
            visits.push(response);
            Dashboard.renderDashboard(visits);
        });
    }

    static getAllAndRender() {

        API.getAllCards().then(response => {
            visits = response;
            Dashboard.renderDashboard(visits);
        });
    }

    static deleteVisit(id) {
        API.deleteCard(id).then(() => {
            visits = visits.filter(visit => visit.id !== id);
            Dashboard.renderDashboard(visits);
        });
    }
}


class VisitCardiologist extends Visit {
    constructor() {
        super();
        this.normalpressure = document.querySelector('.js_mod_input_npresure').value;
        this.mass = document.querySelector('.js_mod_input_mass').value;
        this.diseases = document.querySelector('.js_mod_input_dis').value;
        this.age = document.querySelector('.js_mod_input_age').value;
    }
}

class VisitTherapist extends Visit {
    constructor() {
        super();
        this.age = document.querySelector('.js_mod_input_age').value;
    }
}

class VisitDentist extends Visit {
    constructor() {
        super();
        this.lastvisitdate = document.querySelector('.js_mod_input_lvisit').value;
    }
}