
const modal = {
    log: `<div class='login-container js_mod_login_container'>
            <a class='free-field'></a>
            <div class='login-content'>
                <div class='wr_mess js_mod_label_warMessage_container'>
                </div>
                <div class='input-group input-group-sm mb-3'>
                    <input type='email' class='form-control log-data js_login_email' placeholder='email' aria-label='Username' aria-describedby='basic-addon1'>
                </div>
                <div class='input-group input-group-sm mb-3'>
                    <input type='password' class='form-control log-data js_login_pass' placeholder='password' aria-label='Username' aria-describedby='basic-addon1'>
                </div>
                <span class='description'>You are trying to log in 'Hospital' application. All rights reserved. Any unauthorized access is punishable with the utmost severity of the law</span>
                
                <button type='button' class='btn btn-outline-success btn-sm login-smb js_login_submit'>Log In</button>
            </div>
        </div>`, 

    add_visit(visitData, isUpdate) {
            return `
            <div class='add-modal-container js_mod_add_visit'>
                    <a class='free-field'></a>
                    <div class='add-modal-content'>
                        <div class='add-visit-modal-drop'>
                            
                            <select class='form-select form-select-sm spec-modal js_mod_dd_doc' aria-label='.form-select-sm example'>
                                <option value=''>specialization</option>
                                <option value='Cardiologist'>Cardiologist</option>
                                <option value='Therapist'>Therapist</option>
                                <option value='Dentist'>Dentist</option>
                            </select>

                            <select class='form-select form-select-sm stat-modal js_mod_dd_status' aria-label='.form-select-sm example'>
                                <option value=''>status</option>
                                <option value='open'>Open</option>
                                <option value='done'>Done</option>
                            </select>

                            <select class='form-select form-select-sm urg-modal js_mod_dd_urgency' aria-label='.form-select-sm example'>
                                <option value=''>urgency</option>
                                <option value='low'>Low</option>
                                <option value='normal'>Normal</option>
                                <option value='high'>High</option>
                            </select>
                        </div>
        
                    <div class='input-group input-group-sm mb-3'>
                        <input type='text' class='form-control log-data js_mod_input_purpose' placeholder='visit purpose' aria-label='any' aria-describedby='basic-addon1' value='${visitData.purpose}'>
                    </div>
        
                    <div class='input-group input-group-sm mb-3'>
                        <textarea class='form-control log-data js_mod_input_short' placeholder='short description' aria-label='With textarea'>${visitData.short}</textarea>
                    </div>
        
                    <div class='input-group input-group-sm mb-3'>
                        <input type='text' class='form-control log-data js_mod_input_fname' placeholder='fullname' aria-label='any' aria-describedby='basic-addon1' value='${visitData.fullname}'>
                    </div>
                    <input type='hidden' id='js_hidden_doc_id' value='${visitData.id}'>
        
                    <div class='js_mod_addl_fields'>
                    </div>
        
                    <div class='add-visit-modal-btns'>
                        <button type='button' class='btn btn-secondary btn-sm js_mod_btn_close'>Close</button>
                        <button type='button' class='btn btn-outline-success btn-sm js_mod_btn_add_visit' data-doctor=''>${isUpdate ? 'Update' : 'Add'} Visit</button>
                        <label class='form-label validation_mess hide'>Base fields must be filled</label>
                    </div>
                </div>
            </div>`
    },  

    addl_cardiologist(visitData) {
        return `
            <div class='input-group input-group-sm mb-3'>
                <input type='text' class='form-control log-data js_mod_input_npresure' placeholder='normal pressure' aria-label='any' aria-describedby='basic-addon1' value='${visitData.normalpressure}'>
            </div>

            <div class='input-group input-group-sm mb-3'>
                <input type='text' class='form-control log-data js_mod_input_mass' placeholder='mass' aria-label='any' aria-describedby='basic-addon1' value='${visitData.mass}'>
            </div>

            <div class='input-group input-group-sm mb-3'>
                <input type='text' class='form-control log-data js_mod_input_dis' placeholder='diseases' aria-label='any' aria-describedby='basic-addon1' value='${visitData.diseases}'>
            </div>

            <div class='input-group input-group-sm mb-3'>
                <input type='text' class='form-control log-data js_mod_input_age' placeholder='age' aria-label='any' aria-describedby='basic-addon1' value='${visitData.age}'>
            </div>`
    }, 

    addl_therapist(visitData) { 
        return `
        <div class='input-group input-group-sm mb-3'>
            <input type='text' class='form-control log-data js_mod_input_age' placeholder='age' aria-label='any' aria-describedby='basic-addon1' value='${visitData.age}'>
        </div>`
    }, 

    addl_dentist(visitData) { 
        return `
        <div class='input-group input-group-sm mb-3'>
            <input type='text' class='form-control log-data js_mod_input_lvisit' placeholder='last visit date' aria-label='any' aria-describedby='basic-addon1' value='${visitData.lastvisitdate}'>
        </div>`
    }, 
}



const components = {
    filter_component: `
    <div class='input-group input-group-sm mb-3'>
           <input type='text' class='form-control js_filter_purpose' placeholder='visit purpose' aria-label='' aria-describedby='button-addon2'>
        </div>
        <div class='btn-group dropend'>
            <button type='button' class='btn btn-secondary btn-second-wid btn-sm filter-drop'>Status </button>
            <button type='button' class='btn btn-secondary dropdown-toggle dropdown-toggle-split btn-sm filter-drop-add' data-bs-toggle='dropdown' aria-expanded='false'>
                <span class='visually-hidden'>Toggle Dropdown</span>
            </button>
            <ul class='dropdown-menu js_filter_status'>
                <li><a class='dropdown-item' data-itemid='open'>Open</a></li>
                <li><a class='dropdown-item' data-itemid='done'>Done</a></li>
            </ul>
        </div>
        <div class='btn-group dropend'>
            <button type='button' class='btn btn-secondary btn-second-wid btn-sm filter-drop'>Urgency</button>
            <button type='button' class='btn btn-secondary dropdown-toggle dropdown-toggle-split btn-sm filter-drop-add' data-bs-toggle='dropdown' aria-expanded='false'>
                <span class='visually-hidden'>Toggle Dropdown</span>
            </button>
            <ul class='dropdown-menu js_filter_urgency'>
                <li><a class='dropdown-item' data-itemid='high'>High</a></li>
                <li><a class='dropdown-item' data-itemid='normal'>Normal</a></li>
                <li><a class='dropdown-item' data-itemid='low'>Low</a></li>
            </ul>
        </div>
        <button type='button' class='btn btn-outline-warning btn-sm js_filter_clearBtn'>Clear Filters</button>`, 

    add_btn: `
    <button type='button' class='btn btn-success btn-sm add_visit js_dash_add_visit'>Add Visit</button>`, 

    err_message: `
    <span class='js_mod_label_warMessage'>wrong credentials!<span>`, 

    dashroard(v) {
        return `
        <div class='card card-collapsed js_dash_visit' style='width: 18rem;' data-id='${v.id}'>
                <div class='card-body'>
                    <h5 class='card-title'>${v.specialization} visit</h5>

                    <div class='card-data'>
                        <span class='fUI'>fullname:</span> <span>${v.fullname}</span></br>
                    </div>

                    <div class='more-card-data hide'>
                        <span class='fUI'>specialization:</span> <span>${v.specialization}</span></br>
                        <span class='fUI'>purpose:</span> <span>${v.purpose}</span></br>
                        <span class='fUI'>short:</span> <span>${v.short}</span></br>
                        <span class='fUI'>urgency:</span> <span>${v.urgency}</span></br>
                        <span class='fUI'>status:</span> <span>${v.status}</span></br>
                    </div>

                    <div class="card-buttons">
                        <a class='btn btn-outline-danger btn-sm btn-delete'>Delete</a>
                        <a class='btn btn-outline-primary btn-sm btn-more'>More</a>
                        <a class='btn btn-outline-primary btn-sm btn-change'>Change It</a>
                    </div>
                </div>
            </div>`
    },

    addl_card(v) {
        return `
            <span class='fUI'>normalpressure:</span> <span>${v.normalpressure}</span></br>
            <span class='fUI'>mass:</span> <span>${v.mass}</span></br>                    
            <span class='fUI'>diseases:</span> <span>${v.diseases}</span></br>
            <span class='fUI'>age:</span> <span>${v.age}</span></br>`
    }, 

    addl_ther(v) {
        return `
            <span class='fUI'>age:</span> <span>${v.age}</span></br>`
    }, 

    addl_dent(v) {
        return `
            <span class='fUI'>lastvisitdate:</span> <span>${v.lastvisitdate}</span></br>`
    },

    no_cards: `<span class='fUI no-cards-mess'>No visits have been added</span>`,

}