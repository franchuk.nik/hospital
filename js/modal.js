
class Modal {
    static renderLogInModal() {

        const lModal = document.querySelector('.loginModal'); 
              lModal.insertAdjacentHTML('afterbegin', modal.log);

        const lBtn = document.querySelector('.js_login_submit'), 
              fField = document.querySelector('.free-field');

              fField.addEventListener('click', () => document.querySelector('.login-container').remove());
              lBtn.addEventListener('click', Auth.checkCred);
    }

    static renderAddVisitModal(visitData = DEFAULT_VISIT_DATA, isUpdate = false) {

        const body = document.querySelector('.body'); 
              body.insertAdjacentHTML('beforeend', modal.add_visit(visitData, isUpdate)); 
              
           
        const modalWin = document.querySelector('.js_mod_add_visit'), 
              allModalF = document.querySelector('.js_mod_add_visit').
                         querySelectorAll('select, textarea, input[type="text"]'), 
              allModalDD = document.querySelector('.js_mod_add_visit').
                         querySelectorAll('option'), 
              addVisit = document.querySelector('.js_mod_btn_add_visit'), 
              close = document.querySelector('.js_mod_btn_close'), 
              warning = document.querySelector('.validation_mess'), 
              docDrop = document.querySelector('.js_mod_dd_doc'); 
              
              
              if (isUpdate) {
                for (let key of allModalDD) {
                    for (let val of [visitData.specialization, visitData.urgency, visitData.status]) {
                        if (key.value === val) {
                            key.setAttribute('selected', ''); 
                        }
                    }
                }
  
                Modal.renderAdditionalAddVisit(docDrop.value, visitData); 
              }
              

              docDrop.addEventListener('input', () => {
                    Modal.renderAdditionalAddVisit(docDrop.value, visitData)});


              addVisit.addEventListener('click', () => {
                    if ([...allModalF].find(e => e.value === '') === undefined) {
                        isUpdate ? ROUTER('update', docDrop.value, visitData.id) : ROUTER('create', docDrop.value); 
                        modalWin.remove(); 
                    } else {
                        warning.classList.toggle('hide');
                    }
                });

              close.addEventListener('click', () => modalWin.remove());
    }


    static renderAdditionalAddVisit(specialization, visitData) {

        const addlFields = document.querySelector('.js_mod_addl_fields');
              addlFields.innerHTML = ``;

        if (specialization === 'Cardiologist') {
              addlFields.insertAdjacentHTML('afterbegin', modal.addl_cardiologist(visitData));
        }
        if (specialization === 'Therapist') {
              addlFields.insertAdjacentHTML('afterbegin', modal.addl_therapist(visitData));
        }
        if (specialization === 'Dentist') {
              addlFields.insertAdjacentHTML('afterbegin', modal.addl_dentist(visitData));
        }
    }

}