const DEFAULT_VISIT_DATA = {
    id: '',
    specialization: '',
    urgency: '',
    status: '',
    purpose: '',
    short: '',
    fullname: '',
    normalpressure: '',
    mass: '',
    diseases: '',
    age: '',
    lastvisitdate: ''
};

let visits = [];

// moderator for CRUD operations for visit
const ROUTER = (action, doctor, visitID) => {
    let vID = visitID;
    return {
        'createCardiologist': () => {
            const cardiologist = new VisitCardiologist();
            cardiologist.sendAndSaveVisit();
        },
        'createTherapist': () => {
            const therapist = new VisitTherapist();
            therapist.sendAndSaveVisit();
        },
        'createDentist': () => {
            const dentist = new VisitDentist();
            dentist.sendAndSaveVisit();
        },
        'updateCardiologist': (vID) => {
            const cardiologist_upd = new VisitCardiologist();
            cardiologist_upd.sendAndEditVisit(vID);
        },
        'updateTherapist': (vID) => {
            const therapist_upd = new VisitTherapist();
            therapist_upd.sendAndEditVisit(vID);
        },
        'updateDentist': (vID) => {
            const dentist_upd = new VisitDentist();
            dentist_upd.sendAndEditVisit(vID);
        },
        'getAll': () => {
            Visit.getAllAndRender();
        },
        'delete': (vID) => {
            Visit.deleteVisit(vID);
        }
    }[action + doctor](vID);

}