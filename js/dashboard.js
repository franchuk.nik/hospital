class Dashboard {

    static renderFilters() {

        const body = document.querySelector('.body'),
             filters = document.querySelector('.filters');
             filters.insertAdjacentHTML('afterbegin', components.filter_component);
             body.insertAdjacentHTML('afterbegin', components.add_btn);

        const addBtn = document.querySelector('.js_dash_add_visit');
            addBtn.addEventListener('click', () => {
            Modal.renderAddVisitModal();
        });
    }

    static renderDashboard(visits) {

        const purposeFil = document.querySelector('.js_filter_purpose'),
            statusFil = document.querySelector('.js_filter_status'),
            urgencyFil = document.querySelector('.js_filter_urgency'),
            clearBtn = document.querySelector('.js_filter_clearBtn');

        const dash = document.querySelector('.js_dash');
        dash.innerHTML = ``;

        if (visits.length === 0) {
            dash.insertAdjacentHTML('afterbegin', components.no_cards);
        } else {
            visits.forEach(v => {
                dash.insertAdjacentHTML('afterbegin', components.dashroard(v));

                const moreCardD = document.querySelector('.more-card-data'),
                    changeBtn = document.querySelector('.btn-change'),
                    deleteBtn = document.querySelector('.btn-delete'),
                    moreBtn = document.querySelector('.btn-more');

                if (v.specialization === 'Cardiologist') {
                    moreCardD.insertAdjacentHTML('beforeend', components.addl_card(v));
                }
                if (v.specialization === 'Therapist') {
                    moreCardD.insertAdjacentHTML('beforeend', components.addl_ther(v));
                }
                if (v.specialization === 'Dentist') {
                    moreCardD.insertAdjacentHTML('beforeend', components.addl_dent(v));
                }

                changeBtn.addEventListener('click', () => {
                    Modal.renderAddVisitModal(v, true);
                });

                deleteBtn.addEventListener('click', () => {
                    ROUTER('delete', '', v.id);
                });

                moreBtn.addEventListener('click', (e) => {
                    document.querySelector(`[data-id="${v.id}"] .more-card-data`).classList.toggle('hide');
                    e.target.closest('.card').classList.toggle('card-collapsed');
                    e.target.closest('.card').classList.toggle('card-expanded');
                });
            });
        }
        ;

        purposeFil.addEventListener('input', FL.setPurpose);
        statusFil.addEventListener('click', FL.setStatus);
        urgencyFil.addEventListener('click', FL.setUrgency);
        clearBtn.addEventListener('click', FL.clearFilter);

    };
}
