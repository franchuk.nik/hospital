class Auth {
    
    static cred = {
        login: 'newtest@m.com', 
        pass: 'test' 
    }
    
    static checkCred() {
        const l = document.querySelector('.js_login_email').value, 
              p = document.querySelector('.js_login_pass').value, 
              logWarMess = document.querySelector('.js_mod_label_warMessage'), 
              logBtn = document.querySelector('.js_login_btnLogin'), 
              logContainer = document.querySelector('.js_mod_login_container'), 
              logWarMessCont = document.querySelector('.js_mod_label_warMessage_container'); 

        if ((l === Auth.cred.login) && (p === Auth.cred.pass)) {
              logBtn.remove(); 
              logContainer.remove(); 
              Dashboard.renderFilters();
            ROUTER('getAll', '', '');
        } else if (!logWarMess) {
            logWarMessCont.insertAdjacentHTML('afterbegin', components.err_message); 
        }
    }
}

document.querySelector('.js_login_btnLogin').addEventListener('click', Modal.renderLogInModal);

