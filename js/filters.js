
class FL {
   
  static filter = {
    purpose: '', 
    status: '',  
    urgency: '', 
  }

  
  static setPurpose() {
    FL.filter.purpose = document.querySelector('.js_filter_purpose').value; 
    FL.filtering(); 
  }

  static setStatus(e) { 
    FL.filter.status = e.target.dataset.itemid;
    FL.filtering(); 
  }

  static setUrgency(e) {
    FL.filter.urgency = e.target.dataset.itemid;
    FL.filtering();  
  } 


  static filtering() { 

    let filtered = visits.filter(v => ( 
      (!FL.filter.urgency || v.urgency === FL.filter.urgency) && 
      (!FL.filter.status || v.status === FL.filter.status) && 
      (!FL.filter.purpose || v.purpose.includes(FL.filter.purpose))
      )).map(el => String(el.id)); 

    [...document.querySelectorAll('.js_dash_visit')].forEach(v => {
      if (!filtered.includes(v.dataset.id)) {
        v.classList.add('hide'); 
      } else {
        v.classList.remove('hide');
      }
    })
  }  


  static clearFilter() {
    document.querySelector('.js_filter_purpose').value = '';
    FL.filter.urgency = ''; 
    FL.filter.status = ''; 
    FL.filter.purpose = ''; 

    FL.filtering();
  }
} 