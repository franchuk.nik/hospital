## Credentials 

login: newtest@m.com  
pass: test
 


## Team
 
1. Pavel Teplinskiy 
2. Sergey Liashenko 
3. Nikolay Franchuk 



## Tasks
1. Pavel Teplinskiy: 'More' button functionality, get and render all cards function, styling optimisation
2. Sergey Liashenko: 'Change it' and 'Delete' buttons functionality, API client
3. Nikolay Franchuk: 'Add visit' button functionality, auth, filtering
